import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

def read_GFD(ticker):
   fn = ticker + ".csv"
   data = pd.read_csv(fn)
   dates = pd.to_datetime(data['Date'], format='%Y-%m-%d')

   sprice = data['Close']
   ts = pd.Series(sprice)
   ts.index = dates
   return ts

def event_lr(stock, edate, L, W):

   n = len(stock)
   for i in range(n):
      if stock.index[i] == edate:
         break
   ei = i
   ei1 = i - (L + W + 1)
   ei2 = i + W + 1

   sa = stock[ei1:ei2]

   sb = sa.pct_change()
   sc = sb.dropna()
   sd = np.log(1+sc)

   return sd, sa

def ols(x, y):
   xbar = np.mean(x)
   ybar = np.mean(y)
   
   dx = x - xbar
   dy = y - ybar

   dx2 = sum(dx*dx)

   bhat = sum(dx*dy)/dx2
   ahat = ybar - bhat*xbar

   n = len(x)
   residual = y - ahat - bhat*x
   sigma2 = sum(residual*residual)/(n-2)
   
   return ahat, bhat, sigma2, xbar, dx2

#########################################################################################
sh = "event"+".csv"
ent = pd.read_csv(sh)
ticker = ent['ticker']
en_date = ent['Date']

for i in range(0,7):
	stock = read_GFD(ticker[i])
	etf = read_GFD('SPY')

	L, W =  240, 10

	edate = en_date[i]
	edate = pd.to_datetime(edate)

	lrstock, ss = event_lr(stock, edate, L, W)
	lretf, es = event_lr(etf, edate, L, W)

	ahat, bhat, sigma2, rmbar, drm2sum = ols(lretf[0:L].values, lrstock[0:L].values)

	ar = lrstock[L:] - (ahat + bhat*lretf[L:])
	ar.name = 'AR'

	drm = lretf[L:] - rmbar
	s2 = sigma2*(1 + 1/L + drm*drm/drm2sum)

	car = ar.cumsum()
	car.name = 'CAR'

	carv = s2.cumsum()
	car_tstat = car/np.sqrt(carv)
	car_tstat.name = 'CAR t Score'

	ar_tstat = ar/np.sqrt(s2)
	ar_tstat.name = 'AR t Score'
z
#########################################################################################
	font = {'family' : 'sans-serif',
        'weight' : 'normal',
        'size'   : 20}

	plt.rc('font', **font)
	fig, ax1 = plt.subplots(figsize=(12,8))

	x = range(-W,W+1)
	ax1.plot(x, car, '-ro')
	ax1.grid(color = 'r', linestyle='dotted', lw=0.5)
	plt.xticks(np.arange(min(x), max(x)+1, 1.0))
	plt.autoscale(enable=True, axis='x', tight=True)
	legend = ax1.legend(loc='lower left', shadow=True)
	ax1.set_ylabel('Cumulative Abnormal Return')

	ax2 = ax1.twinx()
	ax2.plot(x, car_tstat, '-bx')
	ax2.set_ylabel('$t$ Score')
	ax2.grid(color = 'b', linestyle='dashed', lw=0.5)

	plt.xlabel('')
	fig.autofmt_xdate()
	legend = ax2.legend(loc='upper right', shadow=True)

	plt.savefig(ticker[i]+'.png', format = 'png', dpi=3*96)
	
plt.show()
