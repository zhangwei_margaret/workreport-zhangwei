import pandas as pd
import pandas_datareader.data as web
from datetime import datetime, timedelta, date
from pandas_datareader._utils import RemoteDataError
import os
#============================================================
'''
Get list of tickers (stock symbols) from companylist.csv
'''
def getTickerList(ticker_file):
    return pd.read_csv(ticker_file)['Symbol']


'''
Download daily stock prices from Quandl or Morningstar
'''
def downloadSingleStock(ticker,database,filepath):
    start = datetime(2000,1,1)
    end = date.today()
    prices = web.DataReader(ticker, database, start, end)
    out_filename = filepath + ticker +  '.csv'
    prices.to_csv(out_filename)

'''
Download daily stock prices fro multiple stocks
'''
def downloadMultipleStocks(ticker_list,database,filepath):
    for ticker in ticker_list:
        try:
            downloadSingleStock(ticker,database,filepath)
        except RemoteDataError: 
            pass

'''
Collect stock symbols in specific dirs
'''
def collectStockName(filepath):
    stock_list=[]
    for root, dirs, files in os.walk(filepath):
        if files:
            for f in files:
                if 'csv' in f:
                    stock_list.append(f.split('.csv')[0])
    return stock_list


#==================================================================

'''
Please change filepaths here.
'''

ticker_file = 'D:/companylist.csv'
database = 'quandl'
filepath = 'D:/data_quandl/' 

database2 = 'morningstar'
filepath2 = 'D:/data_morningstar'

#====================================================================

'''
Download stock prices data from Quandl
'''

ticker_list = getTickerList(ticker_file)
downloadMultipleStocks(ticker_list,database,filepath)

'''
Get stock names inaccessible to Quandl
'''

download_quandl = collectStockName(filepath)
unsucess_quandl = [ticker for ticker in ticker_list and not in download_quandl]



#====================================================================

'''
Use Morningstar database to download prices data of stocks not accessible to Quandl
'''
downloadMultipleStocks(unsucess_quandl,database2,filepath2)

'''
Get stock names inaccessible to Morningstar
'''

download_morningstar = collectStockName(filepath2)
unsucess_morningstar = [ticker for ticker in unsucess_quandl and not in download_morningstar]
